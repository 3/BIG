# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp5s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   wget vim
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable audio (backend: ALSA)
   sound.enable = true;
# Enable audio (backend: PulseAudio), this will work fine alongside ALSA.
# Realtime support for PulseAudio.
security.rtkit.enable = true;
   hardware.pulseaudio = {
enable = true;
package = pkgs.pulseaudioFull;
support32Bit = true;
# chmod 755 /root
# Before continuing, run nixos-rebuild switch! Then copy default.pa over to
# the specified file and directory name (/root/pulseaudio-custom.conf).
# Open /etc/pulse/default.pa to get the true directory (nix store).
# cp /THE/FILE /root/pulseaudio-custom.conf
configFile = /root/pulseaudio-custom.conf;
daemon.config = {
# If set higher than 2, latency will increase, if lower than 2, programs
# such as OBS Studio will have broken audio. Only use if the CPU cannot
# handle the PA config, and low-latency is still desired.
# If the CPU is too slow, there will be frequent pops, crackles, and/or static 
# noise.
default-fragments = "2";
default-fragment-size-msec = "2";
# Most sound devices degrade below/above 48000; test regardless.
# Pulseeffects has a method of playing a specific HZ freq, this is a
# good method for testing whether 44100 or 48000 is better.
# (Check your audio device manual/spec before doing this)
# Usually the sample rate is noticable at 40-120HZ (bass) on good headphones.
default-sample-rate = "48000";
alternate-sample-rate = "48000";
high-priority = "yes";
nice-level = "-11";
realtime-priority = "10";
realtime-scheduling = "yes";
# Flat volumes raise all applications to 150% if one is set at that volume.
# Keeping this off also keeps PulseAudio in line with ALSA's behavior.
# This will be "no" by default in PulseAudio 14.0.
# https://www.freedesktop.org/wiki/Software/PulseAudio/Notes/14.0/
flat-volumes = "no";
};
};
#! Preferences
boot.initrd.luks.devices."enc".allowDiscards = true;
boot.kernelPackages = pkgs.linuxPackages_latest;
boot.postBootCommands = 
''
"iptables-restore < /root/ipv4.rules";
"ip6tables-restore < /root/ipv6.rules";
'';
boot.kernelModules = [ "kvm-amd" "ip_tables" "ip6_tables" ];

boot.kernel.sysctl = {
# "net.core.default_qdisc" = cake;
# "net.ipv4.tcp_congestion_control" = bbr;
"net.ipv4.tcp_fastopen" = 3; "kernel.timer_migration" = 1;
"vm.swappiness" = 100; "vm.overcommit_memory" = 2; "vm.overcommit_ratio" = 100; };
#fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];
fileSystems = {
"/mnt/hdd" = {
device = "/dev/disk/by-uuid/695f3fe1-f3aa-49cb-85ac-9ff348038d65";
fsType = "btrfs";
};
};
fonts = {
# Figure out the best looking fonts later.
  enableDefaultFonts = true;
  fonts = with pkgs; [
    liberation_ttf
  ];
fontconfig = {
penultimate.enable = true;
defaultFonts.serif = [ "Liberation Serif" ];
defaultFonts.sansSerif = [ "Liberation Sans" ];
};
};
powerManagement = {
cpuFreqGovernor = "performance";
cpufreq.min = 3800000;
};
programs = {
gpaste.enable = true;
# Z shell, provides more auto-completion (useful for nix* commands), and protects against: https://archive.ph/mDOQN
zsh.enable = true;
# Customize QT5 theming on any DE and/or WM.
qt5ct.enable = true;
# Minimalist screen locking tool.
slock.enable = true;
# Put your user in the "adbusers" group to access Android Debug Bridge devices.
adb.enable = true;
# Compiler cache.
ccache.enable = true;
};
services.irqbalance.enable = true;
## Turns off mouse acceleration
services.xserver.libinput.accelProfile = "flat";
zramSwap = {
enable = true;
memoryPercent = 20;
algorithm = "lz4";
swapDevices = 1;
};
# Multilib
fonts.fontconfig.cache32Bit = true;

  # Enable the X11 windowing system.
   services.xserver.enable = true;
   services.xserver.displayManager.startx.enable = true; 
# Force amdgpu for GCN1/2, instead of using radeon.
# Replace cik with si for GCN1
 # boot.kernelParams = [ "radeon.cik_support=0" "amdgpu.cik_support=1" ];
 # services.xserver.videoDrivers = [ "amdgpu" ];
 
# Autologin is only recommended for single-user use; disk encryption is a must if not using 'specialized equipment'.
services.mingetty.autologinUser = "admin";
# Personally never used this; used to extend zsh further, and provides advanced theming options.
# man configuration.nix for more info on how to configure it.
 #programs.zsh.ohMyZsh.enable = true;
users.defaultUserShell = pkgs.zsh;
# X11 GPU acceleration
hardware.opengl = {
enable = true;
driSupport = true;
## "This is currently only supported for the nvidia and ati_unfree drivers, as well as Mesa"
driSupport32Bit = true;
};
## 2D acceleration.
services.xserver.useGlamor = true;
# Enable XFCE
services.xserver.desktopManager.xfce = {
enable = true;
};
# System packages
environment.systemPackages = with pkgs;
[emacs pciutils usbutils openvpn iptables openresolv update-resolv-conf];
environment.variables = {
EDITOR = "emacs"; VISUAL = "emacs"; };
# Security focused
boot.loader.systemd-boot.editor=false;

# EXAMPLES; only these 4 DEs support this!
#environment.gnome3.excludePackages = [ pkgs.gnome3.totem ]
#environment.lxqt.excludePackages = [ pkgs.lxqt.qterminal ]
#environment.mate.excludePackages = [ pkgs.mate.mate-terminal pkgs.mate.pluma ]
#environment.pantheon.excludePackages = [ pkgs.pantheon.elementary-camera ]

services.apcupsd.enable = true;
services.apcupsd.configText =
''
UPSTYPE usb
MINUTES 3
'';
# Deny NixOS from mangling iptable rules; it cannot directly import rules (only use commands),
# and adds its own rules without permission from the user, causing DNS leaking on VPNs. 
networking.firewall.enable = false;

#!
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.admin = {
     isNormalUser = true;
     extraGroups = [ "wheel"  "adbusers"]; # Enable ‘sudo’ for the user.
   };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}

