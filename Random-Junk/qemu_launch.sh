# args: -no-hpet -rtc driftfix=slew -global kvm-pit.lost_tick_policy=discard -cpu host,hv_time,kvm=off,+kvm_pv_unhalt,+kvm_pv_eoi,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_reset,hv_vpindex,hv_runtime,hv_relaxed,hv_synic,hv_stimer,hv_vendor_id=proxmox -machine kernel_irqchip=on

# -drive detect-zeroes=off,cache=none,aio=threads,id=disk,format=raw,file=/mnt/exmaple/VMs-extra/win10.img,if=none -device ahci,id=ahci -device ide-drive,drive=disk,bus=ahci.1 \
# --network network=default,model=e1000e,mac=RANDOM,mtu.size=1500 \
# chrt is very important to making taskset do its job!
chrt -r 1 taskset -c 0-11 /mnt/hdd/Downloads/qemu-pinning/bin/debug/native/x86_64-softmmu/qemu-system-x86_64 \
-nodefaults \
-no-user-config \
-sandbox enable=on,elevateprivileges=deny,obsolete=deny,resourcecontrol=deny,spawn=deny \
  -machine q35,accel=kvm,kernel_irqchip=on,vmport=off,dump-guest-core=off \
-cpu host,migratable=no,topoext=on,hv-vendor-id=AuthenticAMD,hv-relaxed,hv-vapic,hv-spinlocks=0x1fff,hv-vpindex,hv-stimer,hv-runtime,hv-crash,hv-time,hv-synic,hv-stimer,hv-reset,hv-frequencies,hv-tlbflush,hv-ipi,hv-passthrough,+kvm-hint-dedicated,+kvm-pv-eoi,+kvm-pv-unhalt,-kvmclock,+invtsc,-spec-ctrl,-ibpb,-ibrs-all,-mds-no,-rdctl-no,-skip-l1dfl-vmentry,hypervisor=off,kvm=off \
-m 16G \
-smp 12,sockets=1,threads=2,cores=6 \
-rtc clock=vm,base=localtime,driftfix=slew \
-global kvm-pit.lost_tick_policy=delay \
-overcommit cpu-pm=off,mem-lock=off \
-drive if=pflash,format=raw,readonly,file=/usr/share/ovmf/x64/OVMF_CODE.fd \
-drive if=pflash,format=raw,file=/home/admin/VMs/WIN10_VARS.fd \
-drive detect-zeroes=off,discard=unmap,cache=directsync,aio=threads,id=disk,format=raw,file=/home/admin/VMs/win10.img,if=none -device ahci,id=ahci -device ide-hd,drive=disk,bus=ahci.0 \
-audiodev id=pa,driver=pa \
-nic user,model=e1000e \
-global ICH9-LPC.disable_s3=1 \
-global ICH9-LPC.disable_s4=1 \
-global pcie-root-port.x-speed=8 \
-global pcie-root-port.x-width=16 \
-device qemu-xhci,id=xhci \
-usb -device usb-host,bus=xhci.0,vendorid=0x1038,productid=0x1724 \
-usb -device usb-host,bus=xhci.0,vendorid=0x04d9,productid=0x0295 \
-usb -device usb-host,bus=xhci.0,vendorid=0x0a12,productid=0x0001 \
-display none \
-vga none \
-parallel none \
-serial none \
-nographic \
-device pcie-root-port,power_controller_present=off,id=root_port1,chassis=0,slot=0,bus=pcie.0 \
-device vfio-pci,multifunction=on,bus=root_port1,addr=00.0,host=09:00.0 \
-device vfio-pci,bus=root_port1,addr=00.1,host=09:00.1 \

#-vcpu vcpunum=0,affinity=11 -vcpu vcpunum=1,affinity=1 \
#-vcpu vcpunum=2,affinity=2 -vcpu vcpunum=3,affinity=3 \
#-vcpu vcpunum=4,affinity=4 -vcpu vcpunum=5,affinity=5 \
#-vcpu vcpunum=6,affinity=6 -vcpu vcpunum=7,affinity=7 \
#-vcpu vcpunum=8,affinity=8 -vcpu vcpunum=9,affinity=9 \
#-vcpu vcpunum=10,affinity=10 -vcpu vcpunum=11,affinity=11 \
#-display sdl,gl=on \
#-device virtio-vga,edid=on,virgl=on,max_outputs=1,xres=1536,yres=864 \

  # -drive media=cdrom,id=cd1,if=none,file=/mnt/hdd/Downloads/en_windows_10_enterprise_ltsc_2019_x64_dvd_5795bb03.iso \
      # -device ide-cd,bus=ide.1,drive=cd1 \

#  -device ich9-intel-hda,msi=on,bus=pcie.0,addr=1b.0 \
#      -device hda-duplex \
