#!/bin/dash

# man jackd
# jack_control help
USERNAME="$(whoami)"
export JACK_NO_AUDIO_RESERVATION=1
export JACK_PROMISCUOUS_SERVER=$USERNAME

jack_control ds alsa
jack_control dps device hw:0
jack_control dps rate 48000
# Set to what Pulseaudio uses.

# QJackCtl displays the XRUN count, use that to finetune the delay using 'period', and 'nperiods'.
#
# XRUNs are either a buffer underrun or a buffer overrun. In both cases, an audio app was either not
# fast enough to deliver data to JACK2's audio buffer, or not fast enough to process data from
# JACK2's audio buffer. Usually XRUNs are audible as crackles or pops.
#
jack_control dps period 64
jack_control dps nperiods 4

jack_control eps port-max 256
# Maximum number of ports JACK2 can manage; remove line to disable.
jack_control eps client-timeout 500
# Timeout in miliseconds (msec); remove line to disable.

jack_control start
pactl unload-module module-jack-sink
pactl unload-module module-jack-source
pactl load-module module-jack-sink channels=2
# Stereo output (speakers, headsets/headphones).
pactl load-module module-jack-source channels=1
# Mono input (microphone).
pacmd set-default-sink jack_out
pacmd set-default-source jack_in
