#!/bin/dash

systemctl enable lvm2-monitor fcron earlyoom irqbalance dbus.socket dbus-broker zram_swap zram_tmp zram_var_tmp
# Activate the required systemd service units; replace dbus-daemon with dbus-broker for the system bus, since it's more stable (less edge cases) and performs better.
systemctl --global enable dbus-broker
# And for user buses as well.
