#!/bin/dash

OPENRC=$(type rc-service)
SYSTEMD=$(type systemctl)

if [ "$2/$3" = "started/begin" ]; then
    if [ "$OPENRC" ]; then
        rc-service samba start
    elif [ "$SYSTEMD" ]; then
        systemctl start smb.service
    fi
fi

if [ "$2/$3" = "release/end" ]; then
    if [ "$OPENRC" ]; then
        rc-service samba stop
    elif [ "$SYSTEMD" ]; then
        systemctl stop smb.service
    fi
fi
