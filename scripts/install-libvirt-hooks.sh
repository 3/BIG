#!/bin/dash

LIBVIRT="/etc/libvirt/hooks"
DOMAIN="win10"
SAMBA="1"

mkdir -p $LIBVIRT
if [ -f qemu.sh ]; then
    cp "${PWD}"/qemu.sh $LIBVIRT/qemu || exit 1
    # Early exit if privileges aren't escalated; also makes the 'printf' not a lie.
    cp "${PWD}"/hugepages.sh $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/hugepages.sh
    cp "${PWD}"/nice.sh $LIBVIRT/qemu.d/$DOMAIN/started/begin/nice.sh
    cp "${PWD}"/scream.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/scream.sh
    cp "${PWD}"/vfio-isolate.sh $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/vfio-isolate.sh
        if [ "$SAMBA" = 1 ]; then
    	    cp "${PWD}"/samba.sh $LIBVIRT/qemu.d/$DOMAIN/started/begin/samba.sh
    	    ln -f $LIBVIRT/qemu.d/$DOMAIN/started/begin/samba.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/samba.sh
	fi
  else
    cp "${0%/*}"/qemu.sh $LIBVIRT/qemu || exit 1
    cp "${0%/*}"/hugepages.sh $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/hugepages.sh
    cp "${0%/*}"/nice.sh $LIBVIRT/qemu.d/$DOMAIN/started/begin/nice.sh
    cp "${0%/*}"/scream.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/scream.sh
    cp "${0%/*}"/vfio-isolate.sh $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/vfio-isolate.sh
        if [ "$SAMBA" = 1 ]; then
    	    cp "${0%/*}"/samba.sh $LIBVIRT/qemu.d/$DOMAIN/started/begin/samba.sh
    	    ln -f $LIBVIRT/qemu.d/$DOMAIN/started/begin/samba.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/samba.sh
	fi
fi
chmod +x $LIBVIRT/qemu
mkdir -p $LIBVIRT/qemu.d/$DOMAIN/prepare/begin
mkdir -p $LIBVIRT/qemu.d/$DOMAIN/started/begin
mkdir -p $LIBVIRT/qemu.d/$DOMAIN/release/end
ln -f $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/hugepages.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/hugepages.sh
ln -f $LIBVIRT/qemu.d/$DOMAIN/prepare/begin/vfio-isolate.sh $LIBVIRT/qemu.d/$DOMAIN/release/end/vfio-isolate.sh
chmod -R +x $LIBVIRT/qemu.d/$DOMAIN
printf "Operation successful.\n"
