#!/bin/dash

LCORES_HOST="C12-22"
LCORES_GUEST="C0-11"
# Accepts multiple ranges as well: C0-3,9-12

    #vfio-isolate cpuset-delete /machine.slice
    # Fails if the cgroups v1 slice already exists.
    vfio-isolate cpuset-create --cpus $LCORES_HOST /host.slice
    vfio-isolate cpuset-create --cpus $LCORES_GUEST -nlb /machine.slice
    vfio-isolate move-tasks / /host.slice
    vfio-isolate -u /tmp/undo-gov cpu-governor performance $LCORES_GUEST
    vfio-isolate vfio-isolate -u /tmp/undo-irq irq-affinity mask $LCORES_GUEST


if [ "$2/$3" = "release/end" ]; then
    vfio-isolate cpuset-delete /host.slice
    vfio-isolate cpuset-delete /machine.slice
    vfio-isolate restore /tmp/undo-gov
    vfio-isolate restore /tmp/undo-irq
fi
