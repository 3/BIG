#!/bin/dash

BRIDGE="br0"
# The network bridge name.
BACKEND="pulse"
# "alsa" can alternatively be used if there are audio problems.
LATENCY="0"
# No additional audio delay; delay is a last resort for fixing audio problems.
DOMAIN="win10"
# Libvirt virtual machine name.
scream -i $BRIDGE -o $BACKEND -t $LATENCY -v >>/tmp/scream-receiver.log 2>&1 &
virsh start $DOMAIN