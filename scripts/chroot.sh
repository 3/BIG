#!/bin/dash
# Shortest possible version.
# cd /mnt/gentoo
# mount -t proc /proc proc/
# mount -R /sys sys/
# mount --make-rslave sys/
# mount -R /dev dev/
# mount --make-rslave dev/
# chroot /mnt/gentoo /bin/bash

mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
