#!/bin/dash
rc-update add apparmor boot
rc-update add lvm boot
rc-update add zram-init boot
rc-update add auditd default
rc-update add dbus default
# Software dependent on D-Bus will break if D-Bus doesn't automatically start upon request, which happens rarely; this leaves it running in standby at all times (until actively used).
rc-update add earlyoom default
rc-update add fcron default
rc-update add irqbalance default
rc-update add syslog-ng default
