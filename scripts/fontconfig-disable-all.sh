#!/bin/dash

echo Disabling all enabled fontconfig .conf files...
output=$(dir /etc/fonts/conf.d | rg '[1-99]' | xargs)
eselect fontconfig disable "$output"
