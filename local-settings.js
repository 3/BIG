pref("general.config.filename", "TB-CUSTOM.cfg");
// The path cannot be changed.
pref("general.config.obscure_value", 0);
// Allows the .cfg file to be stored in plain text (no obfuscation).
